class AddActiveToPqr < ActiveRecord::Migration[5.1]
  def change
    add_column :pqrs, :active, :boolean 
  end
end
