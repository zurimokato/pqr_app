class TiempoSol < ActiveRecord::Migration[5.1]
  def change
            change_column :pqrs, :tiempo_sol, :timestamps , default: -> { 'CURRENT_TIMESTAMP' }
  end
end
