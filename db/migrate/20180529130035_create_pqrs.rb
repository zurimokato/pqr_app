class CreatePqrs < ActiveRecord::Migration[5.1]
  def change
    create_table :pqrs do |t|
      t.string :nombre
      t.string :apellido
      t.string :correo
      t.string :cdeula
      t.string :telefono
      t.string :tipo
      t.binary :archivo
      t.text :descripcion
      t.timestamp :tiempo_sol
      t.timestamp :tiempo_res

      t.timestamps
    end
  end
end
