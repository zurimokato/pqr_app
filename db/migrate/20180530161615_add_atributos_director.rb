class AddAtributosDirector < ActiveRecord::Migration[5.1]
  def change
      
        add_column :directors, :nombre, :string, limit: 30
        add_column :directors, :apellidos, :string, limit: 30
        add_column :directors, :cedula, :string
        add_column :directors, :telefono, :string 
        add_column :directors, :direccion, :string 
  end
end
