class AddUsarioCaracteres < ActiveRecord::Migration[5.1]
  def change
       
        add_column :users, :nombre, :string, limit: 30
        add_column :users, :apellidos, :string, limit: 30
        add_column :users, :cedula, :string
        add_column :users, :telefono, :string 
        add_column :users, :direccion, :string
        add_column :users, :usuario, :string
    
  end
end
