class AddAtributosJefeArea < ActiveRecord::Migration[5.1]
  def change
        
        add_column :jefe_areas, :nombre, :string, limit: 30
        add_column :jefe_areas, :apellidos, :string, limit: 30
        add_column :jefe_areas, :cedula, :string
        add_column :jefe_areas, :telefono, :string 
        add_column :jefe_areas, :direccion, :string 
  end
end
