Rails.application.routes.draw do
  
  devise_for :jefe_areas
  devise_for :directors
  devise_for :users
  get 'sessions/new'

      resources :pqrs
   

   get "persona/create"
   get "pqr/create"
   get "welcome/index"
   get "welcome/login"
   get "welcome/tramites"
   get    'login'   => 'sessions#new'
   post   'login'   => 'sessions#create'
   delete 'logout'  => 'sessions#destroy'
  
   #get"empleados/index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'welcome#index'
  #root 'welcome#tramitar'
 # root 'empleados#index'
  
end


